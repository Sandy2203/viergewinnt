import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

import java.util.ArrayList;
import java.util.List;

public class Graphics {
    private static final int spalte = 6;
    private static final int reihe = 6;

    public Shape macheSpielbrett() {
        Shape shape = new Rectangle((spalte + 1) * Chip.kachelgroesse, (reihe + 1) * Chip.kachelgroesse);
        for (int j = 0; j < reihe; j++) {
            for (int i = 0; i < spalte; i++) {
                Circle circle = new Circle(Chip.kachelgroesse / 2);
                circle.setCenterX(Chip.kachelgroesse / 2);
                circle.setCenterY(Chip.kachelgroesse / 2);
                circle.setTranslateX(i * (Chip.kachelgroesse + 5) + Chip.kachelgroesse / 4);
                circle.setTranslateY(j * (Chip.kachelgroesse + 5) + Chip.kachelgroesse / 4);

                shape = Shape.subtract(shape, circle);
            }

        }
        shape.setFill(Color.BEIGE);
        return shape;
    }
}