import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Chip extends Circle {
    public static final int kachelgroesse = 80;


    public boolean isRed() {
        return red;
    }

    private final boolean red;

    public Chip(boolean red) {
        super(kachelgroesse / 2, red ? Color.RED : Color.NAVY);
        this.red = red;

        setCenterX(kachelgroesse / 2);
        setCenterY(kachelgroesse / 2);
    }
}
