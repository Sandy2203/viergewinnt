
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.scene.shape.Shape;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;


import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class VierGewinnt extends Application {


    private static final int spalte = 6;
    private static final int reihe = 6;

    private boolean redMove = true;
    private Chip[][] grid = new Chip[spalte][reihe];

    private Pane chipRoot = new Pane();
    Graphics graphics = new Graphics();


    private Parent createContent() {
        Pane root = new Pane();
        root.getChildren().add(chipRoot);


        Shape gridShape = graphics.macheSpielbrett();
        root.getChildren().add(gridShape);
        root.getChildren().addAll(macheSpalten());
        return root;


    }


    public List<Rectangle> macheSpalten() {

        List<Rectangle> list = new ArrayList<>();

        for (int i = 0; i < spalte; i++) {
            Rectangle rectangle = new Rectangle(Chip.kachelgroesse, (reihe + 2) * Chip.kachelgroesse);
            rectangle.setTranslateX(i * (Chip.kachelgroesse + 5) + Chip.kachelgroesse / 4);
            rectangle.setFill(Color.TRANSPARENT);
            rectangle.setOnMouseEntered(e -> rectangle.setFill(Color.TRANSPARENT));
            rectangle.setOnMouseExited(e -> rectangle.setFill(Color.TRANSPARENT));
            final int Spalte = i;
            rectangle.setOnMouseClicked(e -> plaziereChip(new Chip(redMove), Spalte));
            list.add(rectangle);
        }

        return list;

    }


    public void plaziereChip(Chip chip, int spalte) {
        int reihe = VierGewinnt.reihe - 1;
        do {
            if (!getChip(spalte, reihe).isPresent())  //schaut, ob da ein Chip liegt
                break;
            reihe--;
        } while (reihe >= 0);
        if (reihe < 0)
            return;
        grid[spalte][reihe] = chip;

        chipRoot.getChildren().add(chip);
        chip.setTranslateX(spalte * (Chip.kachelgroesse + 5) + Chip.kachelgroesse / 4);

        final int aktReihe = reihe;

        TranslateTransition animation = new TranslateTransition(Duration.seconds(0.3), chip); //lässt die Steine runterfallen
        animation.setToY(reihe * (Chip.kachelgroesse + 5) + Chip.kachelgroesse / 4);
        animation.setOnFinished(e -> {
            if (gameEnded(spalte, aktReihe)) {
                gameOver();
            }
            redMove = !redMove;
        });
        animation.play();
    }

    private boolean gameEnded(int spalte, int reihe) {


        List<Point2D> horizontal = IntStream.rangeClosed(spalte - 3, spalte + 3)
                .mapToObj(c -> new Point2D(c, reihe))
                .collect(Collectors.toList());
        List<Point2D> vertical = IntStream.rangeClosed(spalte - 3, spalte + 3)
                .mapToObj(r -> new Point2D(spalte, r))
                .collect(Collectors.toList());

        Point2D topLeft = new Point2D(spalte - 3, reihe - 3);
        List<Point2D> diagonal1 = IntStream.rangeClosed(0, 6)
                .mapToObj(i -> topLeft.add(i, i))
                .collect(Collectors.toList());

        Point2D botLeft = new Point2D(spalte - 3, reihe + 3);
        List<Point2D> diagonal2 = IntStream.rangeClosed(0, 6)
                .mapToObj(i -> botLeft.add(i, -i))
                .collect(Collectors.toList());


        return checkRange(vertical) || checkRange(horizontal) || checkRange(diagonal1) || checkRange(diagonal2);


    }

    private boolean checkRange(List<Point2D> points) {
        int ergebnis = 0;
        for (Point2D p : points) {
            int spalte = (int) p.getX();
            int reihe = (int) p.getY();

            Chip chip = getChip(spalte, reihe).orElse(new Chip(!redMove));  //sagt, ob sich dort ein Chip befindet
            if (chip.isRed() == redMove) {
                ergebnis++;
                if (ergebnis == 4) {
                    return true;
                }
            } else {
                ergebnis = 0;
            }
        }
        return false;
    }


    private void gameOver() {
        JOptionPane.showMessageDialog(null, "Gewinner: " + (redMove ? "Rot" : "Blau"));

        // JOptionPane.showMessageDialog(null,"Neues Spiel");


    }



    private Optional<Chip> getChip(int spalte, int reihe) {    //prüft, ob der Platz noch frei ist
        if (spalte < 0 || spalte >= VierGewinnt.spalte
                || reihe < 0 || reihe >= VierGewinnt.reihe) {
            return Optional.empty();
        }
        return Optional.ofNullable(grid[spalte][reihe]);

    }


    public void start(Stage stage) throws Exception {
        stage.setScene(new Scene(createContent()));
        stage.show();



    }


    public static void main(String[] args) {
        launch(args);


    }

}


